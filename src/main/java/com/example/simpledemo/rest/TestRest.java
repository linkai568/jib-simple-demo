package com.example.simpledemo.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author linkai
 * @date 2020/4/26
 */
@RestController
public class TestRest {
    @GetMapping
    public String hello() {
        return "hello word!1111";
    }
}
