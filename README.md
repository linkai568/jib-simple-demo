# spring-boot镜像分层构件
参考：https://spring.io/guides/topicals/spring-boot-docker

## spring-boot的jar包拆分
可以直接使用docker构建
```
# syntax = docker/dockerfile:experimental
FROM openjdk:8-jdk-alpine as build
WORKDIR /workspace/app

COPY mvnw .
COPY .mvn .mvn
COPY pom.xml .
COPY src src

RUN --mount=type=cache,target=/root/.m2 ./mvnw install -DskipTests
RUN mkdir -p target/dependency && (cd target/dependency; jar -xf ../*.jar)

FROM openjdk:8-jdk-alpine
VOLUME /tmp
ARG DEPENDENCY=/workspace/app/target/dependency
COPY --from=build ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY --from=build ${DEPENDENCY}/META-INF /app/META-INF
COPY --from=build ${DEPENDENCY}/BOOT-INF/classes /app
ENTRYPOINT ["java","-cp","app:app/lib/*","hello.Application"]
```
注意：
该方法使用buildKit特性（--mount=type=cache），在mac下尝试没成功，看的官方资料目前支持 linux，参考 [Build images with BuildKit](https://docs.docker.com/develop/develop-images/build_enhancements/)
运行方式为：```DOCKER_BUILDKIT=1 docker build -t myorg/myapp .```

后面改了一下dockerfile，采用本地打包的方式来操作：
```
FROM openjdk:8-jdk-alpine as build
WORKDIR /workspace/app

RUN mkdir -p target
COPY target/*.jar target
RUN mkdir -p target/dependency && (cd target/dependency; jar -xf ../*.jar)

FROM openjdk:8-jdk-alpine
VOLUME /tmp
ARG DEPENDENCY=/workspace/app/target/dependency
COPY --from=build ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY --from=build ${DEPENDENCY}/META-INF /app/META-INF
COPY --from=build ${DEPENDENCY}/BOOT-INF/classes /app
ENTRYPOINT ["java","-cp","app:app/lib/*","hello.Application"]
```

## spring-boot-maven-plugin
执行命令：./mvnw spring-boot:build-image
```
<plugin>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-maven-plugin</artifactId>
  <configuration>
    <image>
      <name>myorg.demo</name>
    </image>
  </configuration>
</plugin>
```

使用[Cloud Native Buildpacks](https://buildpacks.io/)构建，可以使镜像有效地分层，还使用CF内存计算器根据运行时的可用容器资源在运行时调整JVM的大小

## Jib
Jib 无需Docker守护程序即可为Java应用程序构建优化的Docker和OCI映像，可作为 Maven 和 Gradle 的插件以及Java库使用。

* 优点
无外部依赖（docker）
直接编译，构建，push 到仓库
优化镜像分层，将依赖库、资源、类等单独分层（也就是将spring-boot生成jar包拆开），每次重新构建和推送发生变更的层

* 参考
[jib-maven-plugin分层构件](https://blog.csdn.net/qq_26769513/article/details/102880731)
[jib-maven-plugin github](https://github.com/GoogleContainerTools/jib/tree/master/jib-maven-plugin)
[jib github](https://github.com/GoogleContainerTools/jib)
[jib-maven-plugin开始文档](https://github.com/GoogleContainerTools/jib/tree/master/jib-maven-plugin#quickstart)
[OCI映像规范](https://github.com/opencontainers/image-spec)

* maven插件：com.google.cloud.tools:jib-maven-plugin

* docker权限配置
第二种：docker本地配置```~/.docker```
第一种：settings.xml
```
# id为容器仓库地址
<server>
  <id>registry.hub.docker.com</id>
  <username>myorg</username>
  <password>...</password>
</server>
```

* 构建
构建到docker仓库：```mvn compile jib:build```
构建到docker daemon：```mvn compile jib:dockerBuild```
默认基础镜像：```gcr.io/distroless/java:8```（注意，该镜像没有bash，不能通过sh连接到容器中）
debug镜像：```gcr.io/distroless/java:debug```（注意，可以通过/busybox/sh连接到容器中）

* maven pom插件配置
```xml
            <plugin>
                <groupId>com.google.cloud.tools</groupId>
                <artifactId>jib-maven-plugin</artifactId>
                <version>2.2.0</version>
                <configuration>
                    <!--基础镜像-->
<!-- <from><image>openjdk:8-jre-alpine</image></from>-->
                    <from><image>gcr.io/distroless/java:debug</image></from>
                    <to>
                        <image>jib/jib-simple-demo</image>
<!-- <auth>-->
<!-- <username>你的账户</username>-->
<!-- <password>你的密码</password>-->
<!-- </auth>-->
                    </to>
                    <!--用没配置安全认证的registry-->
                    <allowInsecureRegistries>true</allowInsecureRegistries>
                    <!-- 如果时packaged则打包成jar放到镜像，否则默认是exploded，会去找.class文件和资源文件 -->
<!-- <containerizingMode>packaged</containerizingMode>-->
                    <container>
                        <jvmFlags>
                            <jvmFlag>-Xms256m</jvmFlag>
                            <jvmFlag>-Xmx512m</jvmFlag>
                            <jvmFlag>-Xdebug</jvmFlag>
                            <jvmFlag>-XX:+UnlockExperimentalVMOptions</jvmFlag>
                            <jvmFlag>-XX:+UseCGroupMemoryLimitForHeap</jvmFlag>
                        </jvmFlags>
                        <mainClass>com.example.simpledemo.SimpleDemoApplication</mainClass>
                        <!--暴露端口-->
                        <ports>
                            <port>8080</port>
                        </ports>
                        <!--构建格式：OCI，Docker-->
                        <format>Docker</format>
                        <!--配置成INHERIT会继承父镜像的entrypoint，如果不配置会默认去找jvmFlags和mainClass配置-->
<!-- <entrypoint>-->
<!-- <arg>INHERIT</arg>-->
<!-- </entrypoint>-->
                        <!--配置容器内容放置根路径-->
<!-- <appRoot>/</appRoot>-->
                        <!--相当于docker的WORKDIR，配置后容器内部文件相互访问可以使用相对路径，该相对路径相对于WORKDIR-->
<!-- <workingDirectory>/</workingDirectory>-->
                        <!--使用该参数将镜像的创建时间与系统时间对其-->
<!-- <useCurrentTimestamp>true</useCurrentTimestamp>-->
                    </container>
                </configuration>
                <executions>
                    <execution>
<!-- <phase>package</phase>-->
<!-- <goals>-->
<!-- <goal>build</goal>-->
<!-- <goal>dockerBuild</goal>-->
<!-- </goals>-->
                    </execution>
                </executions>
            </plugin>
```

* 命令行的方式带入jvm参数
```
mvn compile jib:build \
    -Djib.allowInsecureRegistries=true \
    -Djib.to.image=registry.xxx.com/isc/isc-struct-data-linux:latest \
    -Djib.to.auth.username=xxx\
    -Djib.to.auth.password=xxx\
    -Djib.container.jvmFlags=-server,-Dspring.cloud.nacos.config.namespace=xxx,-Dspring.cloud.nacos.discovery.namespace=xxx,-Dspring.cloud.nacos.config.server-addr=10.0.0.1:8848,-Dspring.cloud.nacos.discovery.server-addr=10.0.0.1:8848
```

## 持续集成
Concourse: Jenkinsfile
```
resources:
- name: myapp
  type: git
  source:
    uri: https://github.com/myorg/myapp.git
- name: myapp-image
  type: docker-image
  source:
    email: {{docker-hub-email}}
    username: {{docker-hub-username}}
    password: {{docker-hub-password}}
    repository: myorg/myapp

jobs:
- name: main
  plan:
  - task: build
    file: myapp/src/main/ci/build.yml
  - put: myapp-image
    params:
      build: myapp复制
```

Jenkins: Jenkinsfile
```
node {
    checkout scm
    sh './mvnw -B -DskipTests clean package'
    docker.build("myorg/myapp").push()
}
```

## Buildpacks
Spring Boot Maven和Gradle插件使用buildpack的方式与packCLI在以下示例中使用的方式完全相同。主要区别在于，插件用于docker运行构建，pack而不需要。给定相同的输入，结果图像相同。
[pack命令示例](https://github.com/buildpacks/pack)

```pack build myorg/myapp --builder=cloudfoundry/cnb:bionic --path=.```

## Knative
参考：[Knative 简介](https://yq.aliyun.com/articles/658800)
knative 是谷歌开源的 serverless 架构方案，旨在提供一套简单易用的 serverless 方案，把 serverless 标准化。目前参与的公司主要是 Google、Pivotal、IBM、Red Hat，2018年7月24日才刚刚对外发布，当前还处于快速发展的阶段。
knative 是建立在 kubernetes 和 istio 平台之上的，使用 kubernetes 提供的容器管理能力（deployment、replicaset、和 pods等），以及 istio 提供的网络管理功能（ingress、LB、dynamic route等）。
knative 把整个系统分成了三个部分：
Build：构建系统，把用户定义的函数和应用 build 成容器镜像
Serving：服务系统，用来配置应用的路由、升级策略、自动扩缩容等功能
Eventing：事件系统，用来自动完成事件的绑定和触发



## 安全问题
不使用jdk，使用jre
限制他们的功能（最小特权原则）
```
# 限制他们的功能（最小特权原则）
RUN addgroup -S demo && adduser -S demo -G demo
USER demo
```
